"use strict";

let btn = $("#button");

$(window).scroll(function () {
  if ($(window).scrollTop() > 600) {
    btn.addClass("show");
  } else {
    btn.removeClass("show");
  }
});

btn.on("click", function (e) {
  e.preventDefault();
  $("html, body").animate({ scrollTop: 0 }, "300");
});

$(function () {
  $('a[href*="#"]:not([href="#"])').click(function () {
    let target = $(this.hash);
    target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
    if (target.length) {
      $("html, body").animate(
        {
          scrollTop: target.offset().top - 50,
        },
        500
      );
      return false;
    }
  });
});
